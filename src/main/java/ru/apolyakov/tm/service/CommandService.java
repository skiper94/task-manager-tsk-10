package ru.apolyakov.tm.service;

import ru.apolyakov.tm.api.ICommandRepository;
import ru.apolyakov.tm.api.ICommandService;
import ru.apolyakov.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository){
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }
}
