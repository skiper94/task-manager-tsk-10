package ru.apolyakov.tm;

import ru.apolyakov.tm.bootstrap.Bootstrap;

public class App {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
